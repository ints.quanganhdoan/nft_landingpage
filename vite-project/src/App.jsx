import Header from "./layouts/Header"
import BannerLayout from "./layouts/BannerLayout"
import StorySection from "./layouts/StorySection"
import CollectionSection from "./layouts/CollectionSection"
import AcutionSection from "./layouts/AcutionSection"
import CreatorSection from "./layouts/CreatorSection"
import WalletConnectSection from "./layouts/WalletConnectSection"
import Footer from "./layouts/Footer"

function App() {
  return (
    <>
      <Header />
      <BannerLayout />
      <StorySection />
      <CollectionSection />
      <AcutionSection />
      <CreatorSection />
      <WalletConnectSection />
      <Footer />
    </>
  )
}

export default App
