import AvatarGroup from "./AvatarGroup";
import Button from "./Button";

const AcutionCard = ({ data }) => {
    return (
        <div className="mt-[60px] relative isolate" style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <div style={{ position: 'relative' }}>
                <img src={data.avatarImage} alt="acution 1" />
                <span className={`font-font4 text-sm ${data.isContrast} font-semibold absolute top-[25px] left-5`}>
                    Current bid
                </span>
                <span className="absolute top-[49px] left-5 flex">
                    <img src="/AcutionCards/ethereumcoin.svg" alt="coin logo" />
                    <span className="text-tertiaryColor font-semibold p-1">{data.currentBid} ETH</span>
                </span>
                <Button className="rounded-[20px] bg-tertiaryColor py-2 px-5 absolute top-[25px] right-5 text-base font-semibold
                hover:bg-white hover:text-tertiaryColor">
                    Place bid
                </Button>
                <span className={`absolute left-5 bottom-[50px] text-base font-semibold ${data.isContrast}`}>
                    {data.avatarName}
                </span>
                <span className="absolute flex items-center bottom-[18px] left-5">
                    <img src={data.avatarUser} alt="user avatar 1" className="w-6 h-6 mr-2" />
                    <span className="font-font4 font-semibold text-sm text-grayColor">{data.userName}</span>
                </span>
                <span className="absolute bottom-[27px] right-[92px]">
                    <AvatarGroup groupData={data.groupData} key={data.groupData.id} />
                </span>
            </div>
        </div>
    )
}

export default AcutionCard;