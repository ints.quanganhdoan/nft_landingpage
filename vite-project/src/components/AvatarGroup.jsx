import React from 'react';
import { AcutionGroupData1 } from '../data/AcutionGroupData';

const AvatarGroup = ({ groupData }) => {
    return (
        <>
            <div className='flex'>
                {groupData.map((item) => (
                    <div className="w-8 h-8 rounded-full bg-borderGradient p-[1.5px] -ml-3 transform transition hover:-translate-y-1" key={item.id}>
                        <img src={item.imgSrc} alt={item.alt} className='rounded-full w-full h-full shadow-md' />
                    </div>
                ))}
            </div>
        </>
    );
};

export default AvatarGroup;