import React from 'react';

const Button = ({ children, className = '', link = '', ...props }) => {
    // ...props giúp button linh hoạt hơn, ví dụ thêm onClick hay các thuộc tính khác cho button
    return (
        link.length > 0 ?
            <a href={link}>
                <button className={`flex justify-center items-center gap-x-3 text-white ${className || ''}`} {...props}>
                    {children}
                </button>
            </a>
            :
            <button className={`flex justify-center items-center gap-x-3 text-white ${className || ''}`} {...props}>
                {children}
            </button>
    );
};

export default Button;


