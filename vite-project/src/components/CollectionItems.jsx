import React from 'react';

const CollectionItems = ({ item }) => {
    return (
        <ul className='flex items-center -mb-6'>
            <li className='flex items-center' style={{ flex: 3 }}>
                <span className='text-secondaryColor font-font3 mr-[14px]'>{item.ordinal}</span>
                <div className='flex w-[502px] justify-between'>
                    <div className='flex items-center'>
                        <img src={item.imgSource} alt={item.name} className='w-12 h-12 mr-6' />
                        <span className='text-3xl'>{item.name}</span>
                    </div>
                </div>
                <img src={item.imgEth} alt='ETH icon' />
            </li>
            <li className='flex flex-col items-end relative mt-12' style={{ flex: 1 }}>
                <span className={`text-3xl`}>{item.volume} ETH</span>
                <span className={`text-xl mt-5 ${item.extraVolume > 0 ? 'text-greenColor' : 'text-redColor'}`}>
                    {item.extraVolume > 0 ?
                        `+${item.extraVolume.toFixed(2)}%` : `${item.extraVolume.toFixed(2)}%`}
                </span>
            </li>
            <li className='flex flex-col items-end  mt-12' style={{ flex: 1 }}>
                <span className={`text-3xl`}>{item.floorPrice} ETH</span>
                <span className={`text-xl mt-5 ${item.floorPriceExtra > 0 ? 'text-greenColor' : 'text-redColor'}`}>
                    {item.floorPriceExtra > 0 ?
                        `+${item.floorPriceExtra.toFixed(2)}%` : `${item.floorPriceExtra.toFixed(2)}%`}
                </span>
            </li>
            <li style={{ flex: 1 }} className='flex flex-col items-end text-3xl'>
                {item.items.toLocaleString()}
            </li>
        </ul>
    );
};

export default CollectionItems;