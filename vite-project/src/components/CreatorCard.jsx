import React, { useState } from 'react';

const CreatorCard = ({ data }) => {

    const [isFollow, setIsFollow] = useState(false);

    return (
        <div className='relative'>
            <svg xmlns="http://www.w3.org/2000/svg" width="384" height="96" viewBox="0 0 384 96" fill="none">
                <path d="M384 10C384 4.47715 379.523 0 374 0H10C4.47715 0 0 4.47715 0 10V86C0 91.5229 4.47715 96 10 96H331.226C333.339 96 335.397 95.3309 337.106 94.0887L379.88 62.995C382.468 61.1134 384 58.1065 384 54.9063V10Z" fill="white" fillOpacity="0.05" />
                <path d="M374 0.5C379.247 0.5 383.5 4.7533 383.5 10V54.9063C383.5 57.9465 382.045 60.803 379.586 62.5906L336.812 93.6842C335.188 94.8644 333.233 95.5 331.226 95.5H10C4.75329 95.5 0.5 91.2467 0.5 86V10C0.5 4.7533 4.75329 0.5 10 0.5H374Z" stroke="url(#paint0_linear_1_160)" strokeOpacity="0.7" />
                <defs>
                    <linearGradient id="paint0_linear_1_160" x1="42.24" y1="9.03667" x2="59.4006" y2="138.895" gradientUnits="userSpaceOnUse">
                        <stop stopColor="#15BFFD" />
                        <stop offset="1" stopColor="#9C37FD" />
                    </linearGradient>
                </defs>
            </svg>
            <img src={data.avatar} alt="creator avatar" className='absolute left-[25px] top-5' />
            <span className='absolute top-[23px] left-[100px] font-bold text-base'>{data.name}</span>
            <span className='absolute top-[58px] left-[127px] font-bold text-base'>{data.amount} ETH</span>
            <a
                href="#follow"
                className={`absolute top-[26px] right-[25px] font-bold text-xl ${isFollow ? 'text-grayColor' : 'text-tertiaryColor'}  underline underline-offset-4`}
                onClick={() => setIsFollow(!isFollow)}
            >
                {isFollow ? 'Unfollow' : 'Follow'}
            </a>
        </div>
    );
};

export default CreatorCard;