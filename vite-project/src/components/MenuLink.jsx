import React from 'react';


const MenuLink = ({ data }) => {
    return (
        <ul>
            <li className='mb-12 text-3xl font-semibold'>{data.linkHeader}</li>
            <ul className='ml-[26px]'>
                <li className='mb-[26px] text-xl'><a href={`#/${data.firstLinkItem}`}>{data.firstLinkItem}</a></li>
                <li className='mb-[26px] text-xl'><a href={`#/${data.secondLinkItem}`}>{data.secondLinkItem}</a></li>
                <li className='mb-[26px] text-xl'><a href={`#/${data.thirdLinkItem}`}>{data.thirdLinkItem}</a></li>
                <li className='mb-[26px] text-xl'><a href={`#/${data.fourthLinkItem}`}>{data.fourthLinkItem}</a></li>
            </ul>
        </ul>
    );
};

export default MenuLink;