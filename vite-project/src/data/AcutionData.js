import { AcutionGroupData1, AcutionGroupData2, AcutionGroupData3, AcutionGroupData4, AcutionGroupData5, AcutionGroupData6 } from "./AcutionGroupData"

export const AcutionData = [
    {
        id: 1,
        isContrast: 'text-black',
        currentBid: 7.8,
        avatarImage: '/AcutionCards/Acution1.png',
        avatarName: 'Golden Hour',
        avatarUser: '/AcutionCards/userava1.png',
        userName: 'John Doe',
        groupData: AcutionGroupData1
    },
    {
        id: 2,
        currentBid: 3.9,
        avatarImage: '/AcutionCards/Acution2.png',
        avatarName: 'Blue Panda',
        avatarUser: '/AcutionCards/userava2.png',
        userName: 'Ikema sjakw',
        groupData: AcutionGroupData2
    },
    {
        id: 3,
        currentBid: 2.5,
        avatarImage: '/AcutionCards/Acution3.png',
        avatarName: 'colourfull cat',
        avatarUser: '/AcutionCards/userava3.png',
        userName: 'Dmd Gahn',
        groupData: AcutionGroupData3
    },
    {
        id: 4,
        isContrast: 'text-black',
        currentBid: 7.8,
        avatarImage: '/AcutionCards/Acution1.png',
        avatarName: 'Golden Hour',
        avatarUser: '/AcutionCards/userava1.png',
        userName: 'John Doe',
        groupData: AcutionGroupData4
    },
    {
        id: 5,
        currentBid: 3.9,
        avatarImage: '/AcutionCards/Acution2.png',
        avatarName: 'Blue Panda',
        avatarUser: '/AcutionCards/userava2.png',
        userName: 'Ikema sjakw',
        groupData: AcutionGroupData5
    },
    {
        id: 6,
        currentBid: 2.5,
        avatarImage: '/AcutionCards/Acution3.png',
        avatarName: 'colourfull cat',
        avatarUser: '/AcutionCards/userava3.png',
        userName: 'Dmd Gahn',
        groupData: AcutionGroupData6
    },
]