export const AcutionGroupData1 = [
    {
        id: 1,
        imgSrc: '/AcutionCards/avatar_group_1/avatar1.png',
        alt: 'First User Group1'
    },
    {
        id: 2,
        imgSrc: '/AcutionCards/avatar_group_1/avatar2.png',
        alt: 'Second User Group1'
    },
    {
        id: 3,
        imgSrc: '/AcutionCards/avatar_group_1/avatar3.png',
        alt: 'Third User Group1'
    },
    {
        id: 4,
        imgSrc: '/AcutionCards/avatar_group_1/avatar4.png',
        alt: 'Fourth User Group1'
    },
]

export const AcutionGroupData2 = [
    {
        id: 5,
        imgSrc: '/AcutionCards/avatar_group_2/avatar5.png',
        alt: 'First User Group2'
    },
    {
        id: 6,
        imgSrc: '/AcutionCards/avatar_group_2/avatar6.png',
        alt: 'Second User Group2'
    },
    {
        id: 7,
        imgSrc: '/AcutionCards/avatar_group_2/avatar7.png',
        alt: 'Third User Group2'
    },
    {
        id: 8,
        imgSrc: '/AcutionCards/avatar_group_2/avatar8.png',
        alt: 'Fourth User Group2'
    },
]

export const AcutionGroupData3 = [
    {
        id: 9,
        imgSrc: '/AcutionCards/avatar_group_3/avatar1.png',
        alt: 'First User Group2'
    },
    {
        id: 10,
        imgSrc: '/AcutionCards/avatar_group_3/avatar2.png',
        alt: 'Second User Group2'
    },
    {
        id: 11,
        imgSrc: '/AcutionCards/avatar_group_3/avatar3.png',
        alt: 'Third User Group2'
    },
    {
        id: 12,
        imgSrc: '/AcutionCards/avatar_group_3/avatar4.png',
        alt: 'Fourth User Group2'
    },
]

export const AcutionGroupData4 = [
    {
        id: 13,
        imgSrc: '/AcutionCards/avatar_group_1/avatar1.png',
        alt: 'First User Group1'
    },
    {
        id: 14,
        imgSrc: '/AcutionCards/avatar_group_1/avatar2.png',
        alt: 'Second User Group1'
    },
    {
        id: 15,
        imgSrc: '/AcutionCards/avatar_group_1/avatar3.png',
        alt: 'Third User Group1'
    },
    {
        id: 16,
        imgSrc: '/AcutionCards/avatar_group_1/avatar4.png',
        alt: 'Fourth User Group1'
    },
]

export const AcutionGroupData5 = [
    {
        id: 17,
        imgSrc: '/AcutionCards/avatar_group_2/avatar5.png',
        alt: 'First User Group2'
    },
    {
        id: 18,
        imgSrc: '/AcutionCards/avatar_group_2/avatar6.png',
        alt: 'Second User Group2'
    },
    {
        id: 19,
        imgSrc: '/AcutionCards/avatar_group_2/avatar7.png',
        alt: 'Third User Group2'
    },
    {
        id: 20,
        imgSrc: '/AcutionCards/avatar_group_2/avatar8.png',
        alt: 'Fourth User Group2'
    },
]

export const AcutionGroupData6 = [
    {
        id: 21,
        imgSrc: '/AcutionCards/avatar_group_3/avatar1.png',
        alt: 'First User Group2'
    },
    {
        id: 22,
        imgSrc: '/AcutionCards/avatar_group_3/avatar2.png',
        alt: 'Second User Group2'
    },
    {
        id: 23,
        imgSrc: '/AcutionCards/avatar_group_3/avatar3.png',
        alt: 'Third User Group2'
    },
    {
        id: 24,
        imgSrc: '/AcutionCards/avatar_group_3/avatar4.png',
        alt: 'Fourth User Group2'
    },
]