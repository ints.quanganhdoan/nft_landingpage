export const dataCollections24h = [
    {
        ordinal: 1,
        imgSource: '/CollectionImages/avatar1.png',
        name: 'Bored Ape Yacht Club',
        imgEth: '/CollectionImages/eth.svg',
        volume: 230.38,
        extraVolume: 50.20,
        floorPrice: 12.38,
        floorPriceExtra: 50.20,
        items: 138
    },
    {
        ordinal: 2,
        imgSource: '/CollectionImages/avatar2.png',
        name: 'Mutant Ape Yacht Club',
        imgEth: '/CollectionImages/eth.svg',
        volume: 138.38,
        extraVolume: 28.20,
        floorPrice: 76.38,
        floorPriceExtra: 70.10,
        items: 122
    },
    {
        ordinal: 3,
        imgSource: '/CollectionImages/avatar3.png',
        name: 'Otherdeed for Otherside',
        imgEth: '/CollectionImages/eth.svg',
        volume: 80.18,
        extraVolume: 50.20,
        floorPrice: 91.38,
        floorPriceExtra: -32.20,
        items: 192
    },
    {
        ordinal: 4,
        imgSource: '/CollectionImages/avatar4.png',
        name: 'Pudgy Penguins',
        imgEth: '/CollectionImages/eth.svg',
        volume: 291.38,
        extraVolume: -49.20,
        floorPrice: 61.38,
        floorPriceExtra: 60.20,
        items: 22000
    },
    {
        ordinal: 5,
        imgSource: '/CollectionImages/avatar5.png',
        name: 'Quirkies Originals',
        imgEth: '/CollectionImages/eth.svg',
        volume: 81.38,
        extraVolume: -15.20,
        floorPrice: 23.12,
        floorPriceExtra: 50.20,
        items: 2321
    },
    {
        ordinal: 6,
        imgSource: '/CollectionImages/avatar6.png',
        name: 'Doodle',
        imgEth: '/CollectionImages/eth.svg',
        volume: 340.21,
        extraVolume: 50.20,
        floorPrice: 12.38,
        floorPriceExtra: -21.10,
        items: 2721
    },
    {
        ordinal: 7,
        imgSource: '/CollectionImages/avatar7.png',
        name: 'Quirkies Originals',
        imgEth: '/CollectionImages/eth.svg',
        volume: 263.82,
        extraVolume: 42.20,
        floorPrice: 45.35,
        floorPriceExtra: -13.13,
        items: 712
    },
]

//deep copy
export const dataCollections7d = JSON.parse(JSON.stringify(dataCollections24h));
export const dataCollectionsAllTime = JSON.parse(JSON.stringify(dataCollections24h));

dataCollections7d.sort((a, b) => b.volume - a.volume);
dataCollectionsAllTime.sort((a, b) => b.items - a.items);


dataCollections7d.forEach((item, index) => item.ordinal = index + 1);
dataCollectionsAllTime.forEach((item, index) => item.ordinal = index + 1);