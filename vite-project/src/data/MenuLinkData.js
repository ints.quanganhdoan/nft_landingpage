export const MenuLinkData = [
    {
        id: 1,
        linkHeader: 'Marketplace',
        firstLinkItem: 'Explore',
        secondLinkItem: 'NFTs',
        thirdLinkItem: 'Collectibles',
        fourthLinkItem: 'Virtuallyreally'
    },
    {
        id: 2,
        linkHeader: 'Company',
        firstLinkItem: 'About Us',
        secondLinkItem: 'Support',
        thirdLinkItem: 'Careers',
        fourthLinkItem: 'Contact us'
    },
    {
        id: 3,
        linkHeader: 'Resources',
        firstLinkItem: 'Partners',
        secondLinkItem: 'Blogs',
        thirdLinkItem: 'Help Center',
        fourthLinkItem: 'Live Action'
    },
]