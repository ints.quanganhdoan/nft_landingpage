import React, { useEffect } from 'react';
import Slider from "react-slick";
import AcutionCard from '../components/AcutionCard';
import { AcutionData } from '../data/AcutionData';


const AcutionSection = () => {

    let slideAmount;
    if (AcutionData.length <= 3) {
        slideAmount = AcutionData.length;
    }
    else {
        slideAmount = 3;
    }


    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: slideAmount,
        slidesToScroll: 1,
        prevArrow: <img src="/AcutionCards/arrow-left.svg" alt="left arrow" className='w-[50px] h-[50px]' />,
        nextArrow: <img src="/AcutionCards/arrow-right.svg" alt="left arrow" className='w-[50px] h-[50px]' />
    };

    return (
        <section>
            <div className="wrapper mt-[150px] relative">
                <h1 className='text-center font-font4 text-5xl'>Live Acution</h1>
                <p className='text-center text-base mt-5'>The largest and unique Super rare NFT marketplace<br />For crypto-collectibles</p>
                <div className='absolute w-[407px] h-[613px] rounded-full bg-[#2A3E84] top-24 left-36 blur-[175px]'></div>
                <Slider {...settings}>
                    {AcutionData.map((item) => (
                        <AcutionCard data={item} key={item.id} />
                    ))}
                </Slider>
            </div>
        </section>
    );
};

export default AcutionSection;