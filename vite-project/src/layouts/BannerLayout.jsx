import React from 'react';
import Button from '../components/Button';

const totalBannerData = [
    {
        total: 26,
        name: "Artwork"
    },
    {
        total: 18,
        name: "Aucation"
    },
    {
        total: 8,
        name: "Artist"
    }
]

const BannerLayout = () => {
    return (
        <section className='relative isolate'>
            <div className="wrapper mt-[150px] flex gap-x-[164px] items-center" >
                <div className='max-w-[738px] w-full'>
                    <h1 className='text-[100px] leading-snug font-font2 font-bold'>
                        Discover <br /> Collect and <br /> Sell NFT
                    </h1>
                    <p className='text-3xl mt-[50px]'>
                        Explore on the world’s best largest NFT marketplace
                    </p>
                    <Button className='mt-[69px] bg-primaryGradient min-w-[250px] h-[60px] text-3xl rounded-2xl  shiny-effect-btn-2'>
                        Explore
                    </Button>
                    <ul className='mt-[50px] flex items-center gap-x-[50px]'>
                        {totalBannerData.map((item) => (
                            <li key={item.name} className='text-center font-font2'>
                                <div className='text-[50px]'>{item.total}K+</div>
                                <div className='text-xl font-medium'>{item.name}</div>
                            </li>
                        ))}
                    </ul>
                </div>
                <div className='flex-1 relative isolate'>
                    <img src="/lineBanner.svg" alt="lineBanner" className='absolute -z-10 -top-14 -right-10' />
                    <img srcSet="/logoBanner.png 2x" alt="logoBanner" className='absolute -top-[200px] right-[100px]
                    min-w-[120px] fill-[#ACDDF0] opacity-75 w-[160px] và h-[160px]' />
                    <img src="/banner.png" alt="banner" className='max-w-[526px] max-h-[614px]' />
                </div>
            </div >
        </section >
    );
};

export default BannerLayout;