import React, { useState } from 'react';
import Button from '../components/Button';

import { dataCollections24h, dataCollections7d, dataCollectionsAllTime } from '../data/CollectionData';
import CollectionItems from '../components/CollectionItems';

const CollectionSection = () => {
    const [activeButton, setActiveButton] = useState('24H');
    const [data, setData] = useState(dataCollections24h);

    const handleClick = (button) => {
        setActiveButton(button);
        switch (button) {
            case '24H':
                setData(dataCollections24h);
                break;
            case '7D':
                setData(dataCollections7d);
                break;
            case 'All Time':
                setData(dataCollectionsAllTime);
                break;
        }
    }

    return (
        <section className='relative isolate'>
            <div className='absolute w-[407px] h-[613px] rounded-full bg-[#2A3E84] top-0 left-5 blur-[175px] -z-10'></div>
            <div className="wrapper mt-[222px]" >
                <div className="flex items-center justify-center">
                    <div className="h-[3px] w-36 bg-leftLineGradient rounded-r-lg"></div>
                    <h1 className="text-[50px] font-bold px-12">Top Collections</h1>
                    <div className="h-[3px] w-36 bg-rightLineGradient rounded-l-lg"></div>
                </div>
                <div className='flex items-start text-3xl mt-[50px]'>
                    {['24H', '7D', 'All Time'].map((label) => (
                        <Button
                            key={label}
                            onClick={() => handleClick(label)}
                            className={`min-w-[150px] h-[60px] mr-[52px] rounded-3xl ${activeButton === label ? 'bg-secondaryColor' : 'bg-primaryColor'}`}
                        >
                            {label}
                        </Button>
                    ))}
                </div>
                <ul className='flex items-start mt-16'>
                    {['Collections', 'volume', 'Floor Price', 'Items'].map((item, index) => (
                        <li key={index} style={{ flex: index === 0 ? 3 : 1 }} className={`text-3xl ${index !== 0 ? "flex justify-end" : ""}`}>{item}</li>
                    ))}
                </ul>
                <hr className='mt-4 border-solid border border-[#505050]' />
                {data.map((item) => <CollectionItems item={item} key={item.ordinal} />)}
            </div>
        </section>
    );
};

export default CollectionSection;