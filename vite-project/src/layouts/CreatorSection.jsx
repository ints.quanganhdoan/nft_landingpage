import React from 'react';
import { useState } from 'react';
import CreatorCard from '../components/CreatorCard';
import Button from '../components/Button';
import { CreatorData } from '../data/CreatorData';

const CreatorSection = () => {
    const [isHovered, setIsHovered] = useState(false);

    const [itemsToShow, setItemsToShow] = useState(9);

    const handleCollapsed = () => {
        if (itemsToShow === 9) {
            setItemsToShow(CreatorData.length);
        } else {
            setItemsToShow(9);
        }
    }

    return (
        <section>
            <div className="wrapper mt-[130px] flex flex-col items-center relative">
                <h1 className='text-[50px] font-bold'>Our Creator</h1>
                <p className='text-center text-base -mt-[6px] font-normal'>
                    The largest and unique Super rare NFT marketplace<br />
                    <span>For crypto-collectibles</span>
                </p>
                <div className='mt-[50px] flex flex-wrap gap-x-[60px] gap-y-[46px] justify-center'>
                    {
                        CreatorData.slice(0, itemsToShow).map((item) => (
                            <CreatorCard data={item} key={item.id} />
                        ))
                    }
                </div>
                <Button
                    className={`border border-tertiaryColor rounded-full w-[140px] h-[50px] p-0 mt-[100px] font-bold ${isHovered === true ? 'bg-tertiaryColor' : ''}`}
                    onMouseEnter={() => setIsHovered(true)}
                    onMouseLeave={() => setIsHovered(false)}
                    onClick={() => handleCollapsed()}
                    disabled={CreatorData.length < 9 ? true : false}
                >
                    <span className={isHovered === true ? 'text-white' : 'text-tertiaryColor'}>
                        {itemsToShow == 9 ? 'Explore more' : 'Show less'}
                    </span>
                </Button>
            </div>
        </section>
    );
};

export default CreatorSection;