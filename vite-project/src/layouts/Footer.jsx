import React from 'react';
import MenuLink from '../components/MenuLink';
import { MenuLinkData } from '../data/MenuLinkData';

const Footer = () => {
    return (
        <section className='h-[952px] bg-footerBackground bg-no-repeat bg-cover -mt-5'>
            <div className='relative flex justify-center'>
                <h1 className='absolute top-40 text-[100px]'>NFT Marketplace</h1>
                <div className='absolute top-[384px] left-[65.5px] flex gap-x-[150px]'>
                    <img src="/Footer/footer_logo.png" alt="footer logo" className='w-[436px] h-[121px] ml-[15px]' />
                    <div className='flex gap-x-[150px]'>
                        {MenuLinkData.map((item) => (
                            <MenuLink data={item} key={item.id} />
                        ))}
                    </div>
                </div>
                <div className='absolute top-[743px] right-[170px]'>
                    <p className='text-3xl font-semibold'>Follow Us</p>
                    <div className='flex mt-7 -ml-20 gap-x-5'>
                        <img src="/Footer/group1.svg" alt="group1" />
                        <img src="/Footer/group2.svg" alt="group2" />
                        <img src="/Footer/group3.svg" alt="group3" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Footer;