import React from 'react';
import Button from '../components/Button';
import WalletIcon from '../components/icons/WalletIcon';

const navMenuData = [
    {
        title: "Home",
        link: "#home",
    },
    {
        title: "Explore",
        link: "#explore",
    },
    {
        title: "Products",
        link: "#product",
    },
    {
        title: "Activity",
        link: "#activity",
    },
    {
        title: "Elements",
        link: "#element",
    },
]

const Header = () => {
    return (
        <>
            <img src="/StarHeader2.svg" alt="star" className='absolute top-20 left-[630px]' />
            <img src="/StarHeader1.svg" alt="star" className='absolute top-20 right-28' />
            <header className='h-[100px] bg-[rgba(0,0,0,.1)] relative z-10'>
                <div className='absolute w-[412px] h-[412px] rounded-full bg-[#2A3E84] top-0 right-0 blur-[175px]'></div>
                <div className='absolute w-[412px] h-[412px] rounded-full bg-[#2A3E84] top-0 left-0 blur-[175px]'></div>
                <div className="wrapper flex justify-between h-full items-center">
                    <a href='/'>
                        <img srcSet="/logo-nft.png 2x" alt="logo" className='relative z-10' />
                    </a>
                    <div className='flex items-center gap-x-[59px]'>
                        <ul className='flex items-center gap-x-[30px]'>
                            {navMenuData.map((item) => (
                                <li key={item.title}>
                                    <a href={item.link} className='text-2xl font-font2 transition hover:underline underline-offset-8'>
                                        {item.title}
                                    </a>
                                </li>
                            )
                            )}
                        </ul>
                        <Button className='bg-primaryColor h-[50px] px-8 text-2xl rounded-2xl shiny-effect-btn' link='#wallet'>
                            <WalletIcon />
                            Wallet
                        </Button>
                    </div>
                </div>
            </header>
        </>
    );
};

export default Header;