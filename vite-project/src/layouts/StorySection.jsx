import React from 'react';
import Button from '../components/Button';

const StorySection = () => {
    return (
        <section className='relative isolate -ml-28'>
            <div className="wrapper mt-[155px] flex gap-x-[160px] items-start" >
                <div className='flex-1 isolate'>
                    <img src="/story.png" alt="story image" className='min-w-[550px] min-h-[607px] -mt-6' />
                </div>
                <div className='max-w-[713px] w-full text-left -ml-20'>
                    <h1 className='text-[50px] font-bold mb-[30px]'>
                        NFT's Story
                    </h1>
                    <p className='text-[25px] mb-[30px]'>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </p>
                    <p className='text-[25px]'>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    </p>
                    <Button className='mt-[69px] bg-primaryGradient min-w-[250px] h-[60px] text-3xl rounded-2xl  shiny-effect-btn-2'>
                        Learn More
                    </Button>
                    <img src="/StarStory1.svg" alt="star" className='absolute right-[8%] top-1/3' />
                    <img src="/StarStory2.svg" alt="star" className='absolute -top-4 right-[20%]' />
                </div>
                <div className='absolute w-[407px] h-[613px] rounded-full bg-[#2A3E84] top-10 right-0 blur-[175px] -z-10'></div>
            </div>
        </section>
    );
};

export default StorySection;