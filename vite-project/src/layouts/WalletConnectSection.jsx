import React from 'react';

const WalletConnectSection = () => {
    return (
        <section className='mt-[154px] relative bg bg-customGray backdrop-blur-[5.5px] h-[528px]'>
            <img src="/WalletConnect/background.png" alt="background" className='absolute top-6 -z-20' />
            <div className='absolute w-[356px] h-[356px] rounded-full bg-[#2A3E84] top-0 left-0 blur-[175px] -z-10'></div>
            <img src="/WalletConnect/person.png" alt="person" className='absolute w-[383px] h-[533px] top-2 object-cover' />
            <img src="/WalletConnect/vortex.png" alt="vortex" className='absolute top-14 left-[370px] w-[380px] blur-sm' />
            <img src="/WalletConnect/star.png" alt="star" className='absolute left-[675px] top-[370px] blur-sm' />
            <div className='w-[540px] flex flex-col justify-start absolute top-36 right-[170px]'>
                <h1 className='text-[50px]'>Why choose us?</h1>
                <p className='mt-[15px] text-xl'>Lorem ipsum dolor sit amet consectetur. Congue eu arcu neque um semper. Eros suspendisse varius enim ultrices  isque et quis ctumst. Feugiat amet velit faucibus amet </p>
                <p className='mt-8 text-xl'>Eu feugiat adipiscing viverrfeugiat. Mollis tellus malesuada massa amet lacinia aliquamid ultrices vitae.</p>
            </div>
        </section>
    );
};

export default WalletConnectSection;