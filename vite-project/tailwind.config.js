/** @type {import('tailwindcss').Config} */
export default {
  mode: "jit",
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        font1: ['Inter', 'sans-serif'],
        font2: ['Jura', 'sans-serif'],
        font3: ['Kite One', 'sans-serif'],
        font4: ['Poppins', 'sans-serif']
      },
      fontWeight: {
        bold: '500px'
      },
      colors: {
        bgColor: "#191E35",
        primaryColor: "#332FD0",
        secondaryColor: "#F52AF5",
        tertiaryColor: "#15BFFD",
        redColor: "#FF0000",
        greenColor: "#00FF57",
        grayColor: "#789EAC"
      },
      backgroundColor: {
        customGray: 'rgba(255, 255, 255, 0.04)'
      },
      backgroundImage: {
        primaryGradient: "linear-gradient(93deg, #F52AF5 4.14%, #DD37F5 31.35%, #219BF2 73.51%, #00ADF2 95.21%);",
        leftLineGradient: "linear-gradient(to left, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 0.981385) 32.37%, rgba(255 ,255 ,225 ,0.286954)99.84%, rgba(255 ,225 ,225 ,0.25)100%)",
        rightLineGradient: "linear-gradient(to right, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 0.981385) 32.37%, rgba(255 ,255 ,225 ,0.286954)99.84%, rgba(255 ,225 ,225 ,0.25)100%)",
        borderGradient: "linear-gradient(152.14deg, rgba(21, 191, 253, 0.7) 9.96%, rgba(156, 55, 253, 0.7) 100%)",
        footerBackground: "url('/Footer/footer_background.png')",
      },
      imageRendering: ['responsive']
    },
  },
  plugins: [],
}